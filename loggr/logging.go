package loggr

import (
	"net/http"
	"net/url"
	"time"

	"github.com/sirupsen/logrus"
)

// loggingResponseWriter decorates a http.ResponseWriter, so it's possible to capture the status code
type loggingResponseWriter struct {
	http.ResponseWriter
	statusCode int
}

// WriteHeader captures the ResponseWriter status code
func (lrw *loggingResponseWriter) WriteHeader(code int) {
	lrw.statusCode = code
	lrw.ResponseWriter.WriteHeader(code)
}

func newLoggingResponseWriter(w http.ResponseWriter) *loggingResponseWriter {
	return &loggingResponseWriter{w, http.StatusOK}
}

// Log wraps a http.Handler and logs it
func Log(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()
		lrw := newLoggingResponseWriter(w)
		decodedURL, _ := url.PathUnescape(r.RequestURI)

		h.ServeHTTP(lrw, r)

		logrus.Infof("%s %s %d %s",
			r.Method, decodedURL, lrw.statusCode, time.Since(start),
		)
	})
}
