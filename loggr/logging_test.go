package loggr

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"time"

	"github.com/sirupsen/logrus"

	"github.com/stretchr/testify/assert"
)

func TestLoggingResponseWriter(t *testing.T) {
	w := httptest.NewRecorder()
	lw := newLoggingResponseWriter(w)

	lw.WriteHeader(http.StatusNotFound)
	assert.Equal(t, lw.statusCode, w.Code)

}

// TODO: write good tests
var tests = []struct {
	request *http.Request
	handler http.HandlerFunc
}{
	{
		httptest.NewRequest("GET", "/", nil),
		func(w http.ResponseWriter, r *http.Request) { time.Sleep(time.Nanosecond) },
	},
	{
		httptest.NewRequest("POST", "/test", nil),
		func(w http.ResponseWriter, r *http.Request) { time.Sleep(time.Microsecond) },
	},
	{
		httptest.NewRequest("PUT", "/test%2F/encoded", nil),
		func(w http.ResponseWriter, r *http.Request) { time.Sleep(time.Second) },
	},
	{
		httptest.NewRequest("DELETE", "/test%3Aencoded/", nil),
		func(w http.ResponseWriter, r *http.Request) { time.Sleep(time.Second) },
	},
}

func TestLogger(t *testing.T) {
	var buf bytes.Buffer
	logrus.SetOutput(&buf)
	defer func() { logrus.SetOutput(os.Stderr) }()

	for _, test := range tests {
		loggingTest := Log(test.handler)
		testWriter := httptest.NewRecorder()

		loggingTest.ServeHTTP(testWriter, test.request)

		assert.Regexp(t,
			`time="\d{4}(-\d{2}){2}T(\d{2}:){2}\d{2}-\d{2}:\d{2}" level=info msg="\w{3,} /.* \d{3} \d{1,}(\.\d{1,})?(µ|m|n)?s"`,
			buf.String(),
		)
	}
}
