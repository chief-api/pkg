package encodr

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

// TODO: make some good tests
var tests = []struct {
	in  Response
	out string
}{
	{
		Response{http.StatusOK, "OK"},
		"{\"status\":200,\"message\":\"OK\"}\n",
	},
	{
		Response{http.StatusNotFound, "Not Found"},
		"{\"status\":404,\"message\":\"Not Found\"}\n",
	},
	{
		Response{http.StatusOK, []string{"test1", "test2", "test3"}},
		"{\"status\":200,\"message\":[\"test1\",\"test2\",\"test3\"]}\n",
	},
	{
		Response{http.StatusOK, struct {
			Field1 string `json:"field1"`
			Field2 int    `json:"field2"`
		}{"test1", 1}},
		"{\"status\":200,\"message\":{\"field1\":\"test1\",\"field2\":1}}\n",
	},
}

func TestJSON(t *testing.T) {
	assert := assert.New(t)

	for _, test := range tests {
		w := httptest.NewRecorder()
		JSON(w, test.in.Status, test.in.Message)

		assert.Equal(appJSON, w.Header().Get(contentType))
		assert.Equal(test.in.Status, w.Code)
		assert.Equal(test.out, w.Body.String())
	}
}
