package encodr

import (
	"encoding/json"
	"net/http"
)

const (
	contentType = "Content-Type"
	appJSON     = "application/json; charset=utf-8"
)

// JSON sets the Content-Type and sends the payload.
func JSON(w http.ResponseWriter, code int, msg interface{}) error {
	w.Header().Set(contentType, appJSON)
	w.WriteHeader(code)
	return json.NewEncoder(w).Encode(Response{code, msg})
}
